<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Redirect;
use App\User;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->middleware('auth');
		if(!$user->isAdmin())
			return redirect("/");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
	{
		$data = Product::all();
        return view('product.index',compact('data'));
    }
	
	public function create()
	{
        return view('product.create');
    }
	
	public function store(Request $request){
		
		$validator = Validator::make($request->all(), [
			"name"    => "required|min:3",
			"is_color_based"    => "required"
		]);
		
		if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput($request->all());
        }
		
		DB::beginTransaction();
		try {
		
			$product = new Product();
			
			$product->name = $request->name;
			$product->is_color_based = $request->is_color_based;
			if($request->size)
				$product->size = serialize($request->size);
			if($request->color)
				$product->color = serialize($request->color);
			$product->user_id = Auth::user()->id;
			
			if($request->hasfile('images')){
				$images = array();
				foreach($request->file('images') as $file)
				{
					$name = time().'.'.$file->extension();
					$file->move(public_path().'/uploads/', $name);  
					$images[] = $name;  
				}
				$product->images = json_encode($images);
			 }
			
			$product->save();
			
			DB::commit();	
			Session::flash('success', 'New product created successfully');
		} catch (\Exception $e) {
			DB::rollback();
			DB::commit();
			throw new \Exception('Exception: '. $e);
		}
		return Redirect::back();
	}
	
	public function edit($id)
	{
		$data = Product::find(base64_decode($id));
        return view('product.edit',compact('data'));
    }
	
	public function update(Request $request, $id){
		
		$validator = Validator::make($request->all(), [
			"name"    => "required|min:3",
			"is_color_based"    => "required"
		]);
		
		if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput($request->all());
        }
		
		DB::beginTransaction();
		try {
		
			$product = Product::find($id);
			
			$product->name = $request->name;
			$product->is_color_based = $request->is_color_based;
			if($request->size)
				$product->size = serialize($request->size);
			if($request->color)
				$product->color = serialize($request->color);
			$product->user_id = Auth::user()->id;
			
			if($request->hasfile('images')){
				$images = array();
				foreach($request->file('images') as $file)
				{
					$name = time().'.'.$file->extension();
					$file->move(public_path().'/uploads/', $name);  
					$images[] = $name;  
				}
				$product->images = json_encode($images);
			 }
			
			$product->save();
			
			DB::commit();	
			Session::flash('success', 'Product updated successfully');
		} catch (\Exception $e) {
			DB::rollback();
			DB::commit();
			throw new \Exception('Exception: '. $e);
		}
		return Redirect::back();
	}
	
}
