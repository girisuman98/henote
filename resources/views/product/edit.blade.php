<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Products</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <!-- Styles -->
        <style>
           .mt-3{
				margin-top:3rem;
			}
			.text-center{
				text-align:center;
			}
			.text-white{
				color:#fff;
			}
			.mt-1{
				margin-top:10px;
			}
        </style>
    </head>
    <body>
        <div class="container mt-3">
            <div class="col-sm-3"></div>
			<div class="col-sm-6">
				<h2>Product Update <button type="button" class="btn btn-primary pull-right"><a href="/" class="text-white">Products List</a></button></h2>
				@if (Session::has('success'))
				   <div class="alert alert-success text-center mt-3"><strong>{{ Session::get('success') }}</strong></div>
				@endif
				@if($errors->any())
					{!! implode('', $errors->all('<div class="alert mt-1 alert-danger text-center">:message</div>')) !!}
				@endif
	        	<form action="/product/update/{{ $data->id }}" method="post" enctype="multipart/form-data">
					@csrf
					<div class="form-group">
					  <label for="first_name">Title:</label>
					  <input type="text" class="form-control" id="name" placeholder="Enter product name" name="name" value="{{ $data->name }}" required>
					</div>
					@php $sizes = $color = array(); if($data->size) $sizes = unserialize($data->size); if($data->color) $color = unserialize($data->color); @endphp
					<div class="form-group">
					  <label for="last_name">Size:</label>
					  <div class="d-flex" style="display:flex;">
						  <input type="text" class="form-control" id="size_1" placeholder="Enter size" value="{{ isset($sizes[0]) ? $sizes[0]:'' }}" name="size[]">
						  <input type="text" class="form-control" id="size_2" placeholder="Enter size" value="{{ isset($sizes[1]) ? $sizes[1]:'' }}" name="size[]">
						  <input type="text" class="form-control" id="size_3" placeholder="Enter size" value="{{ isset($sizes[2]) ? $sizes[2]:'' }}" name="size[]">
						  <input type="text" class="form-control" id="size_4" placeholder="Enter size" value="{{ isset($sizes[3]) ? $sizes[3]:'' }}" name="size[]">
						  <input type="text" class="form-control" id="size_5" placeholder="Enter size" value="{{ isset($sizes[4]) ? $sizes[4]:'' }}" name="size[]">
					  </div>
					</div>
					
					<div class="form-group">
					  <label for="last_name">Color:</label>
					  <div class="d-flex" style="display:flex;">
						  <input type="text" class="form-control" id="color_1" placeholder="Enter color" value="{{ isset($color[0]) ? $color[0]:'' }}" name="color[]">
						  <input type="text" class="form-control" id="color_2" placeholder="Enter color"  value="{{ isset($color[1]) ? $color[1]:'' }}" name="color[]">
						  <input type="text" class="form-control" id="color_3" placeholder="Enter color"  value="{{ isset($color[2]) ? $color[2]:'' }}" name="color[]">
						  <input type="text" class="form-control" id="color_4" placeholder="Enter color"  value="{{ isset($color[3]) ? $color[3]:'' }}" name="color[]">
						  <input type="text" class="form-control" id="color_5" placeholder="Enter color"  value="{{ isset($color[4]) ? $color[4]:'' }}" name="color[]">
					  </div>
					</div>
					<div class="form-group">
					  <label for="images">Images:</label>
					  <input type="file" class="form-control" id="images" name="images[]" multiple />
					</div>
					<div class="form-group">
					  <label for="user_type">Color Type:</label>
					  <select class="form-control" id="is_color_based" name="is_color_based" required>
						  <option value="">Choose</option>
						  <option value="1" {{ $data->is_color_based == "1" ? 'selected="selected"' : '' }}>Color Based Product</option>
						  <option value="0" {{ $data->is_color_based == "0" ? 'selected="selected"' : '' }}>Non Color Based Product</option>
						</select>
					</div>
					<button type="submit" class="btn btn-info">Submit</button>
				</form>
			</div>
		</div>
		
		<script>
			$(document).ready(function() {
				
			} );
		</script>
    </body>
</html>
