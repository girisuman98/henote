<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Products</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <!-- Styles -->
        <style>
           .mt-3{
				margin-top:3rem;
			}
			.text-center{
				text-align:center;
			}
			.text-white{
				color:#fff;
			}
        </style>
    </head>
    <body>
        <div class="container mt-3">
            <div class="col-sm-12">
				<h2>Products<button type="button" class="btn btn-primary pull-right"><a href="/product/create" class="text-white">Create Product +</a></button></h2>
				@if(count($data) > 0)
				<table id="products" class="table table-striped table-bordered" style="width:100%">
				<thead>
					<tr>
						<th>Title</th>
						<th>Is Color Based?</th>
						<th>Sizes</th>
						<th>Colors</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				@foreach($data as $item)
					<tr>
						<td>{{ $item->name }}</td>
						<td>@if($item->is_color_based) Yes @else No @endif</td>
						<td>{{ implode(",", unserialize($item->size)) }}</td>
						<td>{{ implode(",", unserialize($item->color)) }}</td>
						<td><a href="/product/edit/{{ base64_encode($item->id) }}">Edit</a></td>
					</tr>
				@endforeach
				</tbody>
				<tfoot>
					<tr>
						<th>Title</th>
						<th>Is Color Based?</th>
						<th>Sizes</th>
						<th>Colors</th>
						<th>Action</th>
					</tr>
				</tfoot>
			</table>
				@else
					<div class="alert alert-danger text-center">Sorry, no data found</div>
				@endif
			</div>
        </div>
		<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
		<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>
		<script>
			$(document).ready(function() {
				$('#products').DataTable();
			} );
		</script>
    </body>
</html>
