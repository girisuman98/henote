<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("/login");
});

Auth::routes();

Route::group(['middleware' => 'auth'], function(){

	Route::get('/home', 'HomeController@index')->name('home');
	Route::get("/products", "ProductController@index");
	Route::get("/product/create", "ProductController@create");
	Route::post("/product/store", "ProductController@store");
	Route::get("/product/edit/{id}", "ProductController@edit");
	Route::post("/product/update/{id}", "ProductController@update");
	
});